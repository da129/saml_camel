settings = JSON.parse(File.read("config/saml/#{Rails.env}/settings.json"))
# NOTE there are two saml dirs, 1 in config and 1 in dummy. They need to be in
# both places so users can run their tests, but the gem can develop with it's
# own tests as well

SamlCamel::Engine.routes.draw do
  get "/" => "saml#index"
  get "/attributes" => 'saml#attr_check'
  get "/failure" => 'saml#failure'
  get "/metadata" => 'saml#metadata'
  get "/testAuthn" => 'saml#force_authn' if settings.dig('settings', 'test_auth_path')
  post "/consumeSaml" => "saml#consume"
  post "/consumeSaml/rawResponse" => "saml#raw_response"
  post "/logout" => "saml#logout"
end
