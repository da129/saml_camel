Rails.application.routes.draw do
  mount SamlCamel::Engine => "/saml"
  root 'corgi#index'
  get '/show/:name' => 'corgi#show', as: 'show_corgi'
end
